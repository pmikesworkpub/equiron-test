package equiron.test;


import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import ch.qos.logback.classic.Logger;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.LoggerFactory;

/**
 *
 * @author m.padorin
 */
public class Task2Test{

    public Task2Test() {
    }

    private final Task2<String, String> task2 = new Task2();

    @Test
    public void testSomeMethod() throws InterruptedException, ExecutionException {
        Logger task2Logger = (Logger) LoggerFactory.getLogger(Task2.class);

        // create and start a ListAppender
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();

        // add the appender to the logger
        task2Logger.addAppender(listAppender);
        
        Function<String, String> func1 = (t) -> {
            return " value = " + t + " ; thread = " + Thread.currentThread().getName();     
        };
        
        new Thread() {
            @Override
            public void run() {
                task2.compute("str1", func1);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    java.util.logging.Logger.getLogger(Task2Test.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();
        new Thread() {
            @Override
            public void run() {
                task2.compute("str2", func1);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    java.util.logging.Logger.getLogger(Task2Test.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();
        new Thread() {
            @Override
            public void run() {
                task2.compute("str1", func1);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    java.util.logging.Logger.getLogger(Task2Test.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }.start();
        
        Thread.sleep(150);

        List<ILoggingEvent> logsList = listAppender.list;
        assertTrue(logsList.get(0).getMessage().contains("calculate"));
        assertTrue(logsList.get(1).getMessage().contains("calculate"));
        assertTrue(logsList.get(2).getMessage().contains("cache detected"));
    }

}
