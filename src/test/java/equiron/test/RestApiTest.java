package equiron.test;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author m.padorin
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RestApiTest {

    @Autowired
    private TestRestTemplate restTemplate;
    
    private final String documentUrl = "http://localhost:8080/document";
    private static final HttpHeaders HEADERS = new HttpHeaders();

    @BeforeAll
    public static void init() {
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
    }

    @Test
    public void documentSuccess() throws JSONException {
        JSONObject jsonObject = new JSONObject("{\"seller\":\"123534251\",\"customer\":\"648563524\",\"products\":[{\"name\":\"milk\",\"code\":\"2364758363546\"},{\"name\":\"water\",\"code\":\"3656352437590\"}]}");
        HttpEntity<String> request = new HttpEntity<>(jsonObject.toString(), HEADERS);
        ResponseEntity<String> testResult = restTemplate.postForEntity(documentUrl, request, String.class);
        assertEquals(HttpStatus.OK, testResult.getStatusCode());
    }
    
    @Test ()
    public void documentNullChecker() throws JSONException {
        JSONObject jsonObject = new JSONObject("{\"seller\":\"\",\"customer\":\"\",\"products\":[{\"name\":\"milk\",\"code\":\"2364758363546\"},{\"name\":\"water\",\"code\":\"3656352437590\"}]}");
        HttpEntity<String> request = new HttpEntity<>(jsonObject.toString(), HEADERS);
        ResponseEntity<String> testResult = restTemplate.postForEntity(documentUrl, request, String.class);
        assertEquals(461, testResult.getStatusCodeValue());
    }
    
    @Test ()
    public void productNullChecker() throws JSONException {
        JSONObject jsonObject = new JSONObject("{\"seller\":\"123534251\",\"customer\":\"648563524\",\"products\":[{\"name\":\"milk\",\"code\":\"\"},{\"name\":\"water\",\"code\":\"3656352437590\"}]}");
        HttpEntity<String> request = new HttpEntity<>(jsonObject.toString(), HEADERS);
        ResponseEntity<String> testResult = restTemplate.postForEntity(documentUrl, request, String.class);
        assertEquals(461, testResult.getStatusCodeValue());
    }
    
    @Test ()
    public void documentLengthChecker() throws JSONException {
        JSONObject jsonObject = new JSONObject("{\"seller\":\"123534251\",\"customer\":\"6485635\",\"products\":[{\"name\":\"milk\",\"code\":\"2364758363546\"},{\"name\":\"water\",\"code\":\"3656352437590\"}]}");
        HttpEntity<String> request = new HttpEntity<>(jsonObject.toString(), HEADERS);
        ResponseEntity<String> testResult = restTemplate.postForEntity(documentUrl, request, String.class);
        assertEquals(460, testResult.getStatusCodeValue());
    }
}
