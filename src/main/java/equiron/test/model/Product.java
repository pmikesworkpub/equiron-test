package equiron.test.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author snowbear
 */
@AllArgsConstructor
@Data
public class Product {

    @NotEmpty
    String name;
    @NotEmpty
    @Size (min = 13, max = 13)
    String code;
   
}
