package equiron.test.model;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author snowbear
 */
@AllArgsConstructor
@Data
public class Document {

    @NotEmpty
    @Size (min = 9, max = 9)
    String seller;
    @NotEmpty
    @Size (min = 9, max = 9)
    String customer;
    @Valid
    List<Product> products;
    
}
