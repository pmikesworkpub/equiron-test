package equiron.test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author snowbear
 * @param <K>
 * @param <V>
 */
public class Task2<K, V> {
    
    Logger logger = LoggerFactory.getLogger(Task2.class);

    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final Map<K, Future<V>> CAHCE = new ConcurrentHashMap();

    public synchronized Future<V> compute(K k, Function<K, V> f) {
        if (CAHCE.containsKey(k)) {
            logger.info("cache detected");
            return CAHCE.get(k);
        }
        Future<V> result =  executor.submit(() -> {
            return f.apply(k);
        });
        logger.info("calculate");
        CAHCE.put(k, result);
        return result;
    }
}
