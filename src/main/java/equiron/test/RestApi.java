package equiron.test;

import equiron.test.model.Document;
import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * rest api предоставляет возможность отправить документ
 *
 * @author snowbear
 */
@RestController
public class RestApi {

    Logger logger = LoggerFactory.getLogger(RestApi.class);

    @PostMapping("/document")
    public ResponseEntity<?> postDocument(@RequestBody Document document) {
  

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Document>> violations = validator.validate(document);
        for (ConstraintViolation<Document> violation : violations) {
            if ( NotEmpty.class.equals(violation.getConstraintDescriptor().getAnnotation().annotationType())) {
                return ResponseEntity.status(461).build();
            }
        }
        for (ConstraintViolation<Document> violation : violations) {
            if ( Size.class.equals(violation.getConstraintDescriptor().getAnnotation().annotationType())) {
                return ResponseEntity.status(460).build();
            }
        }
        logger.info(document.toString());
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
