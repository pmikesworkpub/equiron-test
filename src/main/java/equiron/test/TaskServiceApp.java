package equiron.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author snowbear
 */
@SpringBootApplication
public class TaskServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(TaskServiceApp.class, args);
    }
}
